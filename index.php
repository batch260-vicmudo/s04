<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04: Access Modifiers and Encapsulation</title>
		<style>div.groove {border-style: groove;}</style>
	</head>

	<body>
		<h1>Access Modifiers</h1>

		<h3>Building Variables</h3>
		<!-- <p><?php //echo $building->name; ?></p> -->

		<!-- <p><?php //echo $building->floors; ?></p> -->
		<!-- <p><?php //echo $building->address; ?></p> -->

		<h3>Condominium Variables</h3>
		<p><?php echo $condominium->getName(); ?></p>

		
		<h3>Encapsulation</h3>
		<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>

		
		<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>

		<h3>Mini Activity</h3>
		<p>The building has <?php echo $building->getfloors(); ?> floors</p>
		
		<p>The condominium now has <?php echo $condominium->getfloors(); ?> floors</p>
		<hr/>

		<div class="groove">
		<h1>Activity</h1>
		<h3>Building</h3>
		<?php $building->setName('Caswyn Building') ?>
		<p>The name of the building is <?php echo $building->getName(); ?>.</p>
		<?php $building->setFloors(8) ?>
		<p>The <?php echo $building->getName(); ?> has <?php echo $building->getfloors(); ?> floors.</p>
		<?php $building->setAddress('Timog Avenue, Quezon City, Philippines') ?>
		<p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>
		<?php $building->setName('Caswynn Complex') ?>
		<p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>
		<!-- Condominium -->
		<h3>Condominium</h3>
		
		
		<?php $condominium->setName('Enzo Condo') ?>
		<p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
		<?php $condominium->setFloors(5) ?>
		<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getfloors(); ?> floors.</p>
		<?php $condominium->setAddress('Buendia Avenue, Makati City, Philippines') ?>
		<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>
		<?php $condominium->setName('Enzo Tower') ?>
		<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>

		</div>

	</body>

</html>