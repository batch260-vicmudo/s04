<?php

class Building {

	//Properties
	protected $name;
	protected $floors;
	protected $address;

	//Constructor
	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
	// building name getter and setter
	public function getName() {
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}
	// building floors getter and setter
	public function getFloors() {
		return $this->floors;
	}

	public function setFloors($floors){
		$this->floors = $floors;
	}
	// building address getter and setter
	public function getAddress() {
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}

}

class Condominium extends Building{

	//Write the getName and setName after defining the $name property.
	// These methods serve as the intermediary in accessing the object's properties.
	// These methods therefore define whether an object's property can be accessed or changed.
	// The methods are called getters and setters and implement the encapsulation of an object.

	//set can add and update
	//get is retrieve

	//getters and setters

	public function getName() {
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	//activity floors getter and setter
	public function getFloors(){
		return $this->floors;
	}
	public function setFloors($floors){
		$this->floors = $floors;
	}

	//address getter and setter
	public function getAddress(){
		return $this->address;
	}
	public function setAddress($address){
		$this->address = $address;
	}

}


$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condominium', 5, 'Buendia Avenue, Makati City, Philippines');